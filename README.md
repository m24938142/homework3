# Operating system lab homework 3
## Homework Description
1. Creat a branch 
    * use command : git branch [your student ID]  
    * for example : git branch M10915029
2. Change branch 
    * use command : git checkout [your student ID]  
    * for example : git checkout M10915029
3. Please add your student ID at the bottom of this file, please enter the order in front
4. Creat an empty file in this folder
    * file name : studentID_date(YYYYMMDD)_time(hhmm)
    * for example : M10915029_20210407_1136
5. Please enter your student ID in quotation marks when committing     
    * for example : git commit -m "M10915029"
6. Push the modified content to the remote repository
    * use command : git push --set-upstream origin [your student ID]
    * for example : git push --set-upstream origin M10915029

    * Please use this account, not your own account, otherwise you will not have permission to upload
    * Username : m10915029@mail.ntust.edu.tw  Password : forhomework3
    
7. Screenshot of the current README.md content (including your student ID) 
8. Screenshot of the current directory status (including files added by yourself)
9. Upload two screenshots to moodle

* Do not remove or modify any content and file added by others
* Version control system can track revision history
* Once this situation is found, the homework will be counted as 0 points

## Student ID
1. M10915029
